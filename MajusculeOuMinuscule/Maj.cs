﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MajusculeMin;

namespace MajusculeOuMinuscule
{
    [TestClass]
    public class Maj
    {
        [TestMethod]
        public void MajOuMin()
        {
            Assert.AreEqual("Bonjour a tous", Program.MajusculeMin("Bonjour a tous"));
        }
    }
}
